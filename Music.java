public class Music {

    // paramètres
    private String title;
    private int duration;
    private Artist[] artistSet;

    // Constructeur
    Music(String title, int duration, Artist[] artistSet) {
        this.title = title;
        this.duration = duration;
        this.artistSet = artistSet;
    }

    // Méthode
    public String getInfos() {
        int minutes = this.duration / 60;
        int seconds = this.duration % 60;
        String infos = title + ' ' + minutes + ':' + seconds + ' ';

        // Parcourir le tableau artistSet
        for (var i = 0; i < artistSet.length; i++) {
            infos += artistSet[i].getFullName() + ' ';
        }

        //System.out.println(infos);
        return(infos);
    }

    // getter
    public int getDuration() {
        return this.duration;
    }
}