public class Player {
  public static void main(String[] args) {

    // Instances des artistes
    Artist bobLennon = new Artist("Bob", "Lennon");
    Artist davidMercury = new Artist("David", "Mercury");
    Artist freddyBowie = new Artist("Freddy", "Bowie");
    // Tests des artistes
    // bobLennon.getFullName();
    // davidMercury.getFullName();
    // freddyBowie.getFullName();


    // instances des musiques
    Music oasis = new Music("Wonderwall", 278, new Artist[]{bobLennon});
    Music rammstein = new Music("Adieu", 278, new Artist[]{davidMercury});
    Music vaundy = new Music("Odoriko", 230, new Artist[]{bobLennon, freddyBowie, davidMercury});
    // Tests des musiques
    // oasis.getInfos();
    // rammstein.getInfos();
    // vaundy.getInfos();

    // instances des playlists
    Playlist aled = new Playlist();
    // Tests des playlists
    aled.add(vaundy);
    aled.add(rammstein);
    aled.add(oasis);
    aled.getTotalDuration();
    aled.remove(0);
    aled.getTotalDuration();
    aled.next();
    aled.next();
    aled.next();
  }
}