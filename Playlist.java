import java.util.ArrayList;

public class Playlist {

    // Paramètres
    private Music currentMusic;
    // ArrayList pour ajouter / supprimer des musiques
    private ArrayList<Music> musicList;

    // Constructeur
    public Playlist() {
        musicList = new ArrayList<Music>();
    }

    // Méthodes
    void add(Music music) {
        if (this.currentMusic == null) {
            this.currentMusic = music;
        }
        this.musicList.add(music);
        System.out.println(music.getInfos());
    }

    void remove(int position) {
        this.musicList.remove(position);
        System.out.println(this.musicList.get(position).getInfos());
    }

    void getTotalDuration() {
        int total = 0;
        for (var music: musicList) {
            total += music.getDuration();
        }
        int minutes = total / 60;
        int seconds = total % 60;
        System.out.println("Durée de la playlist : " + minutes + ":" + seconds);
    }

    // Va chercher la position de la musique actuelle dans le tableau, et affiche la suivante
    void next() {
        int position = this.musicList.indexOf(this.currentMusic);
        // pour gérer les out of bounds + incrémentation de la position :
        if (this.musicList.size() > (position + 1)) {
            position++;
            this.currentMusic = this.musicList.get(position);
        } else {
            this.currentMusic = this.musicList.get(0);
        }
        System.out.println("En train de jouer : " + this.currentMusic.getInfos());
    }
}