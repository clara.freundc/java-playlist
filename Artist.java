public class Artist {

    // paramètres
    private String firstName;
    private String lastName;

    // Constructeur
    public Artist(String firstName, String lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
    }

    // Méthode pour afficher les noms de l'artiste
    public String getFullName() {
            // System.out.println(firstName + ' ' + lastName);
            return(firstName + ' ' + lastName);
    }
}